<?php

namespace App\Service\RabbitMQ;

use App\Entity\News;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class TaskProducerService
 * @package App\Service\RabbitMQ
 */
class TaskProducerService
{
    /**
     * @param News $new
     * @param KernelInterface $kernel
     */
    public static function newsProducer(News $new, KernelInterface $kernel)
    {
        $data = [
            "flag" => "api_news",
            "data" => [
                "id" => $new->getId(),
                "slug" => $new->getSlug(),
                "updatedAt" => $new->getUpdatedAt(),
                "publishedAt" => $new->getPublishedAt(),
                "isActive" => $new->isActive(),
                "isHide" => $new->isHide(),
            ]
        ];
        $kernel->getContainer()->get('old_sound_rabbit_mq.task_producer')->setContentType('application/json');
        $kernel->getContainer()->get('old_sound_rabbit_mq.task_producer')->publish(json_encode($data));
    }
}