<?php

namespace App\Repository;

use App\Entity\News;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * Class NewsRepository
 * @package App\Repository
 */
class NewsRepository extends EntityRepository
{
    /**
     * @return QueryBuilder
     */
    public function queryFindNewsList()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('n')
            ->from(News::class, 'n')
            ->where('n.isActive = true')
            ->andWhere('n.isHide = false')
            ->andWhere('n.publishedAt <= :now')
            ->setParameter('now', new DateTime());
    }

    /**
     * @param string $slug
     * @return ?News
     * @throws NonUniqueResultException
     */
    public function findNew(string $slug): ?News
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('n')
            ->from(News::class, 'n')
            ->where('n.isActive = true')
            ->andWhere('n.slug = :slug')
            ->setParameter('slug', $slug)
            ->andWhere('n.publishedAt <= :now')
            ->setParameter('now', new DateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }
}
