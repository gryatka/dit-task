<?php

namespace App\Controller\Rest;

use App\Service\Rest\NewsService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NewsController
 * @package App\Controller\Rest
 */
class NewsController extends AbstractFOSRestController
{
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), ['jms_serializer' => '?' . SerializerInterface::class,]);
    }

    /**
     * @Rest\Get("/list/news/{limit}/{page}", name="api_get_list_news")
     * @param NewsService $service
     * @param int $page
     * @param int $limit
     * @return Response
     */
    public function receiveNewsList(NewsService $service, int $limit = 20, int $page = 1): Response
    {
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($service->getNewsList($limit, $page), 'json');
        return new Response($json, 200);
    }

    /**
     * @Rest\Get("/news/{slug}", name="api_get_news_item")
     * @param NewsService $service
     * @param string $slug
     * @return Response
     */
    public function receiveNew(NewsService $service, string $slug): Response
    {
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($service->getNew($slug), 'json');
        return new Response($json, 200);
    }
}
