# Тестовое задание

Используя PHP 7.3 и Symfony 4 сделано следующее:

1. Разработана модель данных "Новости".
Slug формируется с помощью Gedmo\Slug
    
2. Создана миграция.

3. Контроллеры.
    1. /Rest/NewsAdminController
        1. Create - /api/create/news [POST]
                Body: { "title": "title",
                "description": "desc",
                "shortDescription": "sdesc",
                "publishedAt": "2020-10-18 21:09:07",
                "isActive": true,
                "isHide": false
                }
        2. Update - /api/update/news/{id} [POST]
                Body: { "title": "title",
                "description": "desc",
                "shortDescription": "sdesc",
                "publishedAt": "2020-10-18 21:09:07",
                "isActive": true,
                "isHide": false
                }
        3. Delete - /api/delete/news/{id} [DELETE]
    2. /Rest/NewsController
        1. Получение списка новостей - /api/list/news/{limit}/{page} [GET]
            1. limit -  Количество элементов на странице, по умолчанию 20
            2. page - страница, по умолчанию первая
        2. Получение новости по slug - /api/news/{slug} [GET]
4. sitemap - /sitemap.xml

### Замечания:
1. При получении Новости по slug, если она  isActive = false, publishedAt больше чем сейчас, возвращается 404.
1. Если новость имеет следующий параметр isActive = true и isHide = true, она не выводится в списке новостей и sitemap, но доступна по slug.
1. Во время методов Create, Update, Delete скидываем в очередь RabbitMQ сообщение, содержащее нашу сущность. Далее по цепочке сообщение идёт к TaskConsumerService, где создём файл sitemap.xml, если его нет, и записываем\обновляем\удаляем элемент.
1. Использовался docker из задания
1. start rabbitmq: sudo docker exec -it task_php sh -c "php bin/console rabbitmq:consumer task"

### Контакты для связи:
telegram: @Gryatka